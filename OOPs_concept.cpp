#include <iostream>
 
using namespace std;
// Base class
class Gym {
    int membership;
    
    protected:
      int yogamember;
    
   public:
      int noOfMember;
      
      //functions to access private memeber
      void setmembership(int memb);
      int getmembership(void);
      
};

//Child function Inheritance Derived class
class yoga:public Gym{
    public:
        void setyogamember(int memb);
        int getyogamember(void);
};

void Gym::setmembership(int memb){
    yogamember = memb;
}

int Gym::getmembership(void){
    return yogamember + noOfMember;
}

void yoga::setyogamember(int memb){
    yogamember = memb;
}
int yoga::getyogamember(void){
        return  yogamember;   
}

int main(){
    Gym member1;
    member1.noOfMember=50;
    member1.setmembership(10);
    cout << "Total number of gym members : " << member1.getmembership()<<endl;
    
    yoga yogmem;
    yogmem.setyogamember(4);
    cout << "Total number of yoga gym members : " << yogmem.getyogamember()<<endl;
    
    return 0;
}
